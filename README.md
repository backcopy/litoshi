# Litoshi

Convert between Litecoin and Litoshi with mathematical arbitrary-precision. 


Litoshi is designed to work on both regular client applications like browsers and also conventional server applications using NodeJS.

## Features

* **Litoshi > Litecoin**
	* Convert a Litoshi number to Litecoin. 

* **Litecoin < Litoshi**
	* Convert a Litecoin number to Litoshi. 

* **String support**
	* Full support for Number and String types.

* **Mathematical precision**
	* Precise output using the big.js library, providing arbitrary-precision decimal arithmetic in JavaScript used by over 2,300,000+ developers.




## Install

```
npm i litoshi 
```

### Installing with browsers
Client-side applications can instantly use the browser version of Litoshi by downloading the [browser version](https://gitlab.com/backcopy/litoshi/blob/master/browser_dist/litoshi.js) or by manually building a browser distribution through Browserify.


## Examples
See a quick demo of various examples below. 

### Convert Litecoin to Litoshi

```
const convert = require('litoshi');

async function convertExample(){
try {
    var result = await convert.toLitoshi(1);
    
        console.log(result); 
        // Result is 100000000
	} catch (error) {
		console.error(`ERROR: ${error}`);
	}
}

convertExample(); 
```

### Convert Litoshi to Litecoin

```
const convert = require('litoshi');

async function convertExample(){
try {
    var result = await convert.toLitecoin(100000000);
    
        console.log(result); 
        // Result is 1
	} catch (error) {
		console.error(`ERROR: ${error}`);
	}
}

convertExample(); 
```

### Browser example 
```
<script src="litoshi.js"></script>
<script>
async function convertExample(){
try {
    var result = await litoshi.toLitecoin(1000);
    
        console.log(result); 
        // Result is 1
	} catch (error) {
		console.error(`ERROR: ${error}`);
	}
}

convertExample(); 
</script>


```


## FAQ

* What is a Litoshi? 
	* Litoshi is to Litecoin as pennies are to the dollar. Except that there are 100,000,000 Litoshi in one Litecoin.

* Why do I need a module when I can just divide or multiply by 100,000,000?
	* Floating point numbers can produce unpredictable results in certain situations.




## Built With

* [satoshi-bitcoin](https://github.com/dawsbot/satoshi-bitcoin#readme) - Initially derived from the satoshi-bitcoin codebase.

* [big.js](https://github.com/MikeMcl/big.js#readme) - Providing arbitrary-precision decimal arithmetic in JavaScript used by over 2,300,000+ developers.


## Tests

* TBD.

## Versioning

* v1.0.0 
	* First release. 

## Authors

* Edin Jusupovic 


## License

LitecoinJS is licensed under the [MIT license](https://gitlab.com/backcopy/litoshi/raw/master/LICENSE).

## Acknowledgments

* Dawson Botsford
	* Creator of satoshi-bitcoin. 
	
* Aditya Yadav
	* Litecoin logo and concept graphics. 